#!/usr/bin/env perl
=pod
    Copyright 2009 Nao Nakashima

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
=cut

use strict;
use warnings;
use LWP::UserAgent;
use URI::URL;
use URI::Escape;
use URI::QueryParam;
use File::Path;
use Getopt::Long;

use Config::General;

# Load config file.
my $config_path = "$ENV{'HOME'}/.bcdownload.rc";
my $conf;
eval {
    $conf = new Config::General($config_path);
};
if ($@) {
    die "[FATAL] Configuration file $config_path cannot be loaded.";
};
my %config = $conf->getall;

my $username = $config{'username'};
my $md5passwd = $config{'md5passwd'};

die "[FATAL] username not defined" if not defined($username);
die "[FATAL] md5passwd not defined" if not defined($md5passwd);

my $pathbase = 'http://bcseries.usenetbinaries.info/free/';
my $amember_url = 'http://amember.usenetbinaries.info/amember/member.php';
my $max_tries = 99999;
my $DEBUG = 0;
my $DRY_RUN = 0;
my $ONLY_VIEW_LINKS = 0;
my $min_date = "00000000";
my @view_urls;

print "[DEBUG] Program started\n" if $DEBUG;

my %options = ("agent", 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)');
my $ua = LWP::UserAgent->new(%options);

$ua->timeout(180);

# Set Cookies
my $phpsessid;
my $cookies = "_amember_ru=$username;_amember_rp=$md5passwd";
$ua->default_header("Cookie" => $cookies);
$ua->cookie_jar( {} );

# Get command line arguments
GetOptions ('min-date|m=s' => \$min_date, #Skip galleries with date less than this.
            'debug|d' => \$DEBUG, #Debug mode.
            'dry-run' => \$DRY_RUN, #Do not actually save any files.
            'only-view-links' => \$ONLY_VIEW_LINKS, #Only print links to views and don`t download anything.
           );
sub fetch;

sub fetch_img {
    my $content = shift;
    my $group = uri_unescape shift;
    my $day = uri_unescape shift;
    my $name = uri_unescape shift;
    my $img = uri_unescape shift;

    my $path = "$group/$day/$name";
    print "[DEBUG] Image path: $path, image name: $img\n" if $DEBUG;

    if (not $DRY_RUN) {
        mkpath $path, {error => \my $err};
        for my $diag (@$err) {
            my ($errpath, $message) = each %$diag;
            die "[FATAL] Problem creating path $errpath: $message";
        }
        open HANDLE, '>', "$path/$img" or die "[FATAL] Cannot open $img for writing";
        print HANDLE ${$content};
        close HANDLE;
    };
}

sub fetch_view {
    my $content = shift;
    my $group = shift;
    my $day = shift;
    my $name = shift;

    # Get picture URL.
    my ($img, $session_id) = (${$content} =~ m"<img[^>]*src=\"read_image\.php\?img=([^&]*)&[^\"]*session_id=([a-z0-9]*)"o);
    unless (defined $img){
        warn "[WARN] Cannot find link to image, skipping";
        return;
    }
    my $url = "${pathbase}read_image.php?img=$img&group=$group&name=$name&day=$day&session_id=$session_id";

    my $target = uri_unescape("$group/$day/$name/$img");
    unless (-s $target) {
        print "[MSG] Downloading image $url\n";
        fetch $url;
    } else {
        print "[NOTE] File $target already exist, skipping\n";
    }

    # Get next picture.
    (my $next) = (${$content} =~ m"<a class=\"mn\" href=\"[^\"]*pic=([^&]*)&[^\"]*\">Next"o);
    # Exit if we reach end of collection.
    return unless defined $next;
    # Construct next page URL
    $url = "${pathbase}view.php?pic=${next}&group=${group}&name=${name}&day=${day}";
    # This is needed because some galleries have strange bug: on some pages the "next" link point to some previous picture.
    unless (grep {$_ eq $url} @view_urls) {
        push @view_urls, $url;
        print "[DEBUG] Currently ".scalar(@view_urls)." urls in anti-loop stack\n" if $DEBUG;
        fetch $url;
    } else {
        warn "[WARN] Loop detected. Skipping $url\n";
        push @view_urls, $url;
    }
}

sub fetch_members {
    my $content = shift;
    my $group = shift;
    my $day = shift;
    # Extract galleries names.
    my @names = (${$content} =~ m"<img src=\"../thumb/[^\"]*/([^\"]*)_pr\.[a-z]*\""og);

    my $name_number=1;
    # Construct URLs to galleries
    foreach my $name (@names) {
        my $url = "${pathbase}view.php?day=${day}&group=${group}&name=${name}";
        print "[MSG] Fetching collection URL: $url\n";
        # Clear anti-loop array
        @view_urls = ();

        unless ($ONLY_VIEW_LINKS) {
            fetch $url;
            print "[MSG] $name_number/".@names." collections in this day done.\n";
        }
        $name_number++;
    }
}

sub parse_members {
    my $content = shift;
    my $group = shift;
    print "[DEBUG] Parsing members URL to find another days\n" if $DEBUG;

    my @days = (${$content} =~ m"<a class=\"pg\" href=\"[^\"]*day=([0-9]*)[^\"]*\""og);

    my $day_number=1;
    my $days=@days;
    foreach my $day (@days) {
        my $url = "${pathbase}members.php?day=${day}&group=${group}";

        if ($day ge $min_date) {
            fetch $amember_url;
            fetch $url;
            print "[MSG] $day_number/$days days done.\n";
            $day_number++;
        } else {
            print "[DEBUG] URL is not in allowed dates range. Skipping. URL: $url\n";
            $days--
        }
    }
}

sub fetch {
    # Generic fetch function
    my $url_string = shift;
    my $url = url $url_string;

    print "[DEBUG] URL string: $url_string\n" if $DEBUG;

    # Extract type of target page: view or members
    (my $page_type) = ($url->epath =~ m"^.*/([^/]*)\.php$"o);
    #"
    print "[DEBUG] URL type: $page_type\n" if $DEBUG;
    # die if don`t know how to parse page with this name.
    unless ($page_type eq "view" || $page_type eq "members" || $page_type eq "read_image" || $page_type eq "member" ) {
        warn "[WARN] Incorrect type of page '$page_type' requested, skipping URL";
        return;
    }

    my $group = uri_escape($url->query_param("group")); #FIXME: If not exist?
    my $day = uri_escape($url->query_param("day"));
    my $name = uri_escape($url->query_param("name"));
    my $img = uri_escape($url->query_param("img"));

    my $response;

    for (my $tries=1; $tries <= $max_tries; $tries++) {
        # Perform a GET request
        $response = $ua->get($url);

        if ($response->is_success) {

            # Set session cookies
            $ua->cookie_jar->extract_cookies($response);
            #print "[DEBUG] Cookie jar: ".$ua->cookie_jar->as_string."\n" if $DEBUG;
            #$ua->cookie_jar->scan(\&set_phpsessid);

            # Something goes wrong? Return!
            unless (defined $group){
                warn "[WARN] 'group' undefined in URL, skipping";
                return;
            }

            if ($page_type eq "members"){

                if (defined $day){
                    # Just recursive fetch child 'view' pages
                    fetch_members \$response->decoded_content, $group, $day;
                } else {
                    # Go extracting views URLs from members page
                    parse_members \$response->decoded_content, $group;
                }
            }
            elsif ($page_type eq "view"){
                unless (defined $day && defined $name){
                    warn "[WARN] 'day' or 'name' undefined in URL, skipping";
                    return;
                }
                fetch_view \$response->decoded_content, $group, $day, $name;
            }
            elsif ($page_type eq "read_image"){
                unless (defined $day && defined $name && defined $img){
                    warn "[WARN] 'day', 'name' or 'img' undefined in URL, skipping";
                    return;
                }
                print "[DEBUG] Name is: $name\n" if $DEBUG;
                fetch_img \$response->decoded_content, $group, $day, $name, $img;
            }
            else {
                warn "[WARN] Unknown page type. Filter of page types incorrectly configured";
            }

            last;

        } else {
                warn "[WARN] Cannot fetch '".$url->as_string."'. Response was: ".$response->status_line;
        }
    }

    warn "[ERR] URL not fetched after $max_tries tries\n" unless ($response->is_success);
}

fetch $amember_url;
fetch shift;
